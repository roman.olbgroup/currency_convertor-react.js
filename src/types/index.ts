interface Currency {
    r030: number;
    txt: string;
    rate: number;
    cc: string;
    exchangedate: string;
}

export type {Currency}

