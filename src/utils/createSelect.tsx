export const createSelect = (
    options: string[], 
    name: string,
    value: string, 
    callback: React.Dispatch<React.SetStateAction<string>>) => {
return  (
    <select name={name} id="" value={value} onChange={(e) => callback(e.target.value)}>
        {options.map((el) => <option key={el} value={el}>{el}</option>)}
    </select>
)}
