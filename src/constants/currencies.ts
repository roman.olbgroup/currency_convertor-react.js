import { Currency } from "../types"

export const currenciesList = ['USD', 'EUR', 'PLN', 'CAD', 'GBP', 'UAH']

export const BASE_CURRENCY: Currency = {
    r030: 0,
    txt: 'Гривня',
    rate: 1,
    cc: 'UAH',
    exchangedate: '',
}