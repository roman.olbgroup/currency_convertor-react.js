import { useEffect, useState } from "react";
import {API_URL, currenciesList, BASE_CURRENCY} from './constants';
import {createSelect} from './utils/createSelect'
import {Currency} from './types'

const  App = () => {
  const [inputCurrencyName, setInputCurrencyName] = useState<string>('UAH')
  const [inputCurrencyValue, setInputCurrencyValue] = useState<number>(1)
  const [oututCurrencyName, setOutputCurrencyName] = useState<string>('USD')
  const [oututCurrencyValue, setOutputCurrencyValue] = useState<number>(1)
  const [rates, setRates] = useState<Currency[]>([])
  const [isLoading, setIsloading] = useState<boolean>(false)
  const [exchangeRate, setExchangeRate] = useState<number>(1)

  useEffect(() => {
    const fetchData = async () => {
      setIsloading(true)
      const response = await fetch(API_URL)
      const data: Currency[] = await response.json()
      setRates([...data.filter(({cc}) => currenciesList.includes(cc)), BASE_CURRENCY])
      setIsloading(false)
    }
    fetchData()
  }, [])

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const amount = parseFloat(e.target.value) || 1  
    setInputCurrencyValue(amount)
    setOutputCurrencyValue(amount * exchangeRate)
  }

  const handleOutputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const amount = parseFloat(e.target.value) || 1  
    setOutputCurrencyValue(amount)
    setInputCurrencyValue(amount / exchangeRate)
  }

  useEffect(() => {
    if (rates.length > 0) {
      const inputRate = rates.find(({cc}) => cc === inputCurrencyName)!['rate'];
      const outputRate = rates.find(({cc}) => cc === oututCurrencyName)!['rate'];
      const newExchangeRate = inputRate / outputRate;
      setExchangeRate(newExchangeRate)
      setOutputCurrencyValue(inputCurrencyValue * newExchangeRate)
    }
  }, [inputCurrencyName, oututCurrencyName, rates, inputCurrencyValue])

  if(isLoading || rates.length === 0) {
    return <div>Loading...</div>
  }

  return (
    <div>
      <h1 className="title">BucksBunny</h1>
      <div>
        <h2>Currency rates on {rates[0]['exchangedate']}</h2>
        <div> 
          {createSelect(currenciesList, 'inputCurr', inputCurrencyName, setInputCurrencyName)}
          <input value={inputCurrencyValue} onChange={handleInputChange} />
        </div>
        <div className="equals">=</div>
        <div>
          {createSelect(currenciesList, 'outputCurr', oututCurrencyName, setOutputCurrencyName)}
        <input value={oututCurrencyValue} onChange={handleOutputChange} />
        </div>
      </div>
    </div>
  );
}

export default App;
